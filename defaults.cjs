function defaults(obj,defaultProps){
    if(typeof obj =="object" && typeof defaultProps =="object"){
        let newobj={};
        for(let key in defaultProps){
            if(obj[key]==undefined){
                newobj[key]=defaultProps[key];

            }
            else{
                newobj[key]=obj[key];
            }
            
        }
        return newobj;
    }
    else{
        return [];
    }
}
module.exports=defaults;